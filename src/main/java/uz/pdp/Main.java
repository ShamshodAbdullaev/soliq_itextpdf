package uz.pdp;

import com.google.gson.Gson;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.sun.xml.internal.stream.writers.UTF8OutputStreamWriter;
import javafx.scene.chart.PieChart;
import org.w3c.dom.Text;
import sun.text.normalizer.UTF16;

import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Gson gson = new Gson();
        File file = new File("e:/soliq.pdf");
        Document document = new Document();
        try {
            OutputStream outputStream = new FileOutputStream(file);
            PdfWriter.getInstance(document,outputStream);
            document.open();

            BaseFont hel = BaseFont.createFont("c:\\Windows\\Fonts\\times.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fonts = new Font(hel,13);
            Font font = new Font(hel,12);
            Font fontSize = new Font(hel,10);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            table.setSpacingAfter(20f);
            float[] tableWidth = {50,50};
            table.setWidths(tableWidth);
            PdfPCell cell = new PdfPCell(new Paragraph());
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            PdfPCell cellHeader = new PdfPCell(new Paragraph("Солиқ солиш объектлари ҳамда солиқ" +
                    "солиш билан боғлик объектлар тўғрисидаги" +
                    "маълумотлар базасини шакллантириш ва" +
                    "юритиш тартиби тўғрисидаги" +
                    "низомга 2-илова",font));
            cellHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellHeader.setBorder(Rectangle.NO_BORDER);
            table.addCell(cellHeader);
            document.add(table);

            Paragraph paragraphTitle = new Paragraph("ЎЗБЕКИСТОН РЕСПУБЛИКАСИ ДАВЛАТ СОЛИҚ ҚЎМИТАСИ\n" +
                    "ГОСУДАРСТВЕННЫЙ НАЛОГОВЫЙ КОМИТЕТ РЕСПУБЛИКИ УЗБЕКИСТАН",fonts);
            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
            paragraphTitle.setSpacingAfter(20f);
            document.add(paragraphTitle);

            Paragraph paragraphGuv = new Paragraph("Солиқ тўловчининг Ўзбекистон Республикаси Давлат солиқ қўмитасида рўйхатга" +
                    "олинганлиги ва унга идентификация рақами берилганлиги тўғрисида\n" +
                    "ГУВОҲНОМА",font);
            paragraphGuv.setAlignment(Element.ALIGN_CENTER);
            paragraphGuv.setSpacingAfter(20f);
            document.add(paragraphGuv);

            Paragraph paragraphUdos = new Paragraph("УДОСТОВЕРЕНИЕ\n" +
                    "о регистрации в Государственном налоговом комитете Республики Узбекистан" +
                    "и присвоении идентификационного номера налогоплательщику",font);
            paragraphUdos.setAlignment(Element.ALIGN_CENTER);
            paragraphUdos.setSpacingAfter(20f);
            document.add(paragraphUdos);

            Paragraph paragraphSoliq = new Paragraph("СОЛИҚ ТЎЛОВЧИ - ЖИСМОНИЙ ШАХС\n" +
                    "(НАЛОГОПЛАТЕЛЬЩИК - ФИЗИЧЕСКОЕ ЛИЦО):",font);
            paragraphSoliq.setAlignment(Element.ALIGN_CENTER);
            paragraphSoliq.setSpacingAfter(10f);
            document.add(paragraphSoliq);

            PdfPTable tableFio = new PdfPTable(2);
            tableFio.setWidthPercentage(100);
            tableFio.setSpacingBefore(5f);
            tableFio.setSpacingAfter(5f);
            float[] fioWidth = {50,50};
            tableFio.setWidths(fioWidth);
            PdfPCell fCell = new PdfPCell(new Paragraph("Фамилияси:\n" +
                    "(Фамилия)",fontSize));
            fCell.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(fCell);
            System.out.print("LASTNAME: ");
            String lastName = scanner.nextLine();
            PdfPCell fCells = new PdfPCell(new Paragraph(lastName.toUpperCase(),fontSize));
            fCells.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(fCells);
            PdfPCell iCell = new PdfPCell(new Paragraph("Исми:\n" +
                    "(Имя)",fontSize));
            iCell.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(iCell);
            System.out.print("NAME: ");
            String  name= scanner.nextLine();
            PdfPCell iCells = new PdfPCell(new Paragraph(name.toUpperCase(),fontSize));
            iCells.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(iCells);
            PdfPCell oCell = new PdfPCell(new Paragraph("Отасини исми:\n" +
                    "(Отчество)",fontSize));
            oCell.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(oCell);
            System.out.print("SURNAME: ");
            String  surName= scanner.nextLine();
            PdfPCell oCells = new PdfPCell(new Paragraph(surName.toUpperCase(),fontSize));
            oCells.setBorder(Rectangle.NO_BORDER);
            tableFio.addCell(oCells);
            document.add(tableFio);

            Paragraph paragraphStip = new Paragraph("Ўзбекистон Республикаси Давлат солиқ қўмитасида рўйхатга олинди ва унга қуйидаги\n" +
                    "солиқ тўловчи идентификация раками (СТИР) берилди:",font);
            paragraphStip.setAlignment(Element.ALIGN_CENTER);
            paragraphStip.setSpacingBefore(4f);
            paragraphStip.setSpacingAfter(10f);
            document.add(paragraphStip);

            Paragraph paragraphInn = new Paragraph("(Зарегистрирован в Государственном налоговом комитете Республики Узбекистан\n" +
                    "и ему присвоен идентификационный номер налогоплательщика (ИНН))",font);
            paragraphInn.setAlignment(Element.ALIGN_CENTER);
            paragraphInn.setSpacingAfter(10f);
            document.add(paragraphInn);

            PdfPTable dataTable = new PdfPTable(3);
            dataTable.setWidthPercentage(100);
            dataTable.setSpacingAfter(10f);
            float[] dataWidth = {30,50,20};
            dataTable.setWidths(dataWidth);
            PdfPCell innCell = new PdfPCell(new Paragraph("508245410"));
            innCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            innCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            dataTable.addCell(innCell);
            PdfPCell stipCell = new PdfPCell(new Paragraph("СТИР берилган сана:\n" +
                    "(Дата присвоения ИНН)",font));
            stipCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            stipCell.setBorder(Rectangle.NO_BORDER);
            dataTable.addCell(stipCell);
            Date date = new Date();
            SimpleDateFormat dataFormats = new SimpleDateFormat("dd.MM.yyyy");
            PdfPCell dataCell = new PdfPCell(new Paragraph(dataFormats.format(date)));
            dataCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            dataCell.setBorder(Rectangle.NO_BORDER);
            dataTable.addCell(dataCell);
            document.add(dataTable);

            PdfPTable dsiTable = new PdfPTable(2);
            dsiTable.setWidthPercentage(100);
            float[] dsiWidth = {70,30};
            dsiTable.setWidths(dsiWidth);
            PdfPCell dsiCell = new PdfPCell(new Paragraph("Рўйхатга олинган ДСИ:\n" +
                    "(Зарегистирован в ГНИ)",fontSize));
            dsiCell.setBorder(Rectangle.NO_BORDER);
            dsiTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            dsiTable.addCell(dsiCell);
            dsiTable.addCell(new Paragraph("MIROBOD TUMAN",fonts));
            document.add(dsiTable);

            Paragraph paragraphUgd = new Paragraph("Ушбу Гувоҳнома Давлат солиқ қўмитасининг интернетдаги расмий сайти\n" +
                    "                    \"орқали шакллантирилган ва чоп этилган",fontSize);
            paragraphUgd.setAlignment(Element.ALIGN_CENTER);
            paragraphUgd.setSpacingBefore(10f);
            document.add(paragraphUgd);

            Paragraph paragraphUgds = new Paragraph("Данное Удостверение сформировано и распечатано через официальный сайт\n" +
                    "Государственного налогового комитета",fontSize);
            paragraphUgds.setAlignment(Element.ALIGN_CENTER);
            paragraphUgds.setSpacingBefore(8f);
            document.add(paragraphUgds);

            PdfPTable diqTable = new PdfPTable(2);
            diqTable.setHorizontalAlignment(Element.ALIGN_CENTER);
            diqTable.setWidthPercentage(100);
            diqTable.setSpacingBefore(5f);
            float[] diqWidth = {50,50};
            diqTable.setWidths(diqWidth);
            PdfPCell diqCell = new PdfPCell(new Paragraph("ДИҚҚАТ!\n" +
                    "Гувоҳноманинг ҳақиқийлигини текшириб кўриш учун " +
                    "ДСҚнинг 'soliq.uz' сайтидаги 'СТИРингизни аниқланг' " +
                    "интерактив хизмати оркали фукаронинг паспорт " +
                    "маълумотларини киритиб текшириш мумкин\n",fontSize));
            diqCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            diqCell.setBorder(Rectangle.NO_BORDER);
            diqCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            diqTable.addCell(diqCell);
            PdfPCell diqCell1 = new PdfPCell(new Paragraph("ВНИМАНИЕ!\n" +
                    "Проверить достоверность данного удостоверения " +
                    "можно на сайте ГНК 'soliq.uz' посредством " +
                    "интерактивной услуги 'Узнай свой ИНН' путем ввода " +
                    "паспортных данных заявителя",fontSize));
            diqCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            diqCell1.setBorder(Rectangle.NO_BORDER);
            diqCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            diqTable.addCell(diqCell1);
            document.add(diqTable);

            PdfPTable sanaTable = new PdfPTable(2);
            sanaTable.setHorizontalAlignment(Element.ALIGN_CENTER);
            sanaTable.setWidthPercentage(100);
            sanaTable.setSpacingBefore(4f);
            float[] sanaWidth = {50,50};
            sanaTable.setWidths(sanaWidth);
            PdfPCell sanaCell = new PdfPCell(new Paragraph("Гувоҳнома чоп этилган сана:\n" +
                    "(Дата распечатки удостоверение)",fontSize));
            sanaCell.setBorder(Rectangle.NO_BORDER);
            sanaTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            sanaTable.addCell(sanaCell);
            SimpleDateFormat dataFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            sanaTable.addCell(new Paragraph(dataFormat.format(date),fontSize));
            document.add(sanaTable);

            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
